# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 09:25:25 2021

@author: lucas
"""

#The private key to the repository is yQwXN3BKo2jCCrxDs-XY

import requests
import sys
import re
from configparser import ConfigParser

config_parser = ConfigParser()
config_parser.read('config.ini')
# url = "https://gitlab.com/"
# project_id = "31417922"

url = config_parser.get('git', 'url')
project_id = config_parser.get('git', 'project_id')
private_token = config_parser.get('git', 'private_token')
branch = config_parser.get('git', 'branch')

###### RMT-1 ######

#API request to get the list of all tags
tag_response = requests.get(url+"/api/v4/projects/"+project_id+"/repository/tags", headers={'PRIVATE-TOKEN': private_token})

#Response in the form of a list of dictionaries
tagList = tag_response.json()

#Private variable and filling of all tags' titles
List_of_versions = []
for tag in tagList :
    version = re.search(r"\d.0.0", tag["name"])
    if version :
        List_of_versions.append(version.group())

#Edition of a public .txt file containing all all tags' titles
versions_file = open("versions.txt", "w")
for version in List_of_versions :
    versions_file.write(version+"\n")
versions_file.close()

###### RMT-2 ######
#Input : tagList
#Output : List_of_commits (private) and commits.txt (public)

#API request to get the list of all the commits
commit_response = requests.get(url+"/api/v4/projects/"+project_id+"/repository/commits", headers={'PRIVATE-TOKEN': private_token}, params={'ref_name': branch})

#Response in the form of a list of maps
commitList = commit_response.json()

#Private variable containing all the titles of the concerned commits
List_of_commits = []

#Edition of a public .txt file containing all the title of the concerned commits
raw_commits_file = open("commits.txt", "w") 

#Specification of the version we want to have access to
found = False
while not found :
    version_number = input("Entrez le numéro de version majeur (ou end pour arrêter) : ")
    if version_number == "end" :
        sys.exit()
    
    n = len(List_of_versions)
    i = 0
    found = False
    while i<n and not found:
        found = List_of_versions[i] == version_number
        i += 1
    
    n = len(tagList)
    
    if found :
        initial_commit_id = tagList[i-1]['commit']['id']
        #if it is the first version we take all commits between this version and the first commit
        if i == n :
            final_commit_id = commitList[-1]["id"]
        #if it isn't then we take all commits between this version and the precedent
        else :
            final_commit_id = tagList[i]['commit']['id']
    else :
        print("Le numéro de version n'est pas valide\n")

#Access to all concerned commits (of the chosen version)

i=0
while commitList[i]['id'] != initial_commit_id:
    i += 1

while commitList[i]['id'] != final_commit_id:
    title_of_commit = commitList[i]['title']
    List_of_commits.append(title_of_commit)
    raw_commits_file.write(title_of_commit+"\n")
    i += 1

raw_commits_file.close()

###### RMT-3 ######
# Input : List_of_commits
# Output : tickets.txt (public)

# Private variable containing all the tickets at the start of the commits' title
tickets_set = set()

for title in List_of_commits:
    reg_ex = config_parser.get('jira', 'id_regex')
    reg_ex.encode('unicode_escape')
    ticket = re.search(reg_ex, title)
    if ticket:
        tickets_set.add(ticket.group())

# Private variable containing all the tickets at the start of the commits' title in a sorted manner
tickets_list = sorted(tickets_set)

#Edition of a public .txt file containing all the tickets at the start of the commits' title in a sorted manner
raw_tickets_file = open("tickets.txt", "w")

for ticket in tickets_list:
    raw_tickets_file.write(ticket + "\n")

raw_tickets_file.close()