# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 09:25:25 2021

@author: lucas
"""

import requests
import re
import argparse
from configparser import ConfigParser

#Creation of the argument parser
args_parser = argparse.ArgumentParser()
args_parser.add_argument('command', choices = ['versions', 'commits', 'releaseNote'])
args_parser.add_argument("version_number", nargs = '?')
args = args_parser.parse_args()

#Extraction of the argument values
command = args.command
version_number = args.version_number

#Creation and readinf of the configuration parser
config_parser = ConfigParser()
config_parser.read('config.ini')

#Extraction of the configuration values
url = config_parser.get('git', 'url')
project_id = config_parser.get('git', 'project_id')
private_token = config_parser.get('git', 'private_token')
branch = config_parser.get('git', 'branch')

reg_ex_version = config_parser.get('git', 'version_regex')
reg_ex_version.encode('unicode_escape') #encoding is necessary to be used in reg_ex

reg_ex_id = config_parser.get('jira', 'id_regex')
reg_ex_id.encode('unicode_escape') #encoding is necessary to be used in reg_ex

def versions() : #RMT-1
    """
    
    Returns 
    -------
    List_of_versions : list
        List of all versions
    
    tagList : dict
        Dict of all tags.
    
    """
    #API request to get the list of all tags - cannot exceed 100 tags
    tag_response = requests.get(url+"/api/v4/projects/"+project_id+"/repository/tags", headers={'PRIVATE-TOKEN': private_token}, params={'ref_name': branch, 'all' : True, 'per_page' : 100}) 
    #Response in the form of a list of dictionaries
    tagList = tag_response.json()
    
    #Creation of the list of versions
    List_of_versions = []
    for tag in tagList :
        version = re.search(reg_ex_version, tag["name"])
        if version :
            List_of_versions.append(version.group())
    
    return List_of_versions, tagList

def commits(List_of_versions, tagList, version_number) : #RMT-2
    """
    
    Parameters
    ----------
    List_of_versions : list
        List of all versions
    
    tagList : dict
        Dict of all tags.
    
    version_number : string
        The number of the version

    Returns
    -------
    List_of_commits : list
        The list of all the commits of the version

    """
    
    #Specification of the version we want to have access to
    max_version_index = len(List_of_versions)
    current_version_index = 0
    while current_version_index < max_version_index and List_of_versions[current_version_index] != version_number:
        current_version_index += 1
    
    date_until = tagList[current_version_index]['commit']['committed_date']
    
    #If it is the first version we take all commits between this version and the first commit
    if current_version_index + 1 == max_version_index :
        
        #API request to get the list of all the commits - cannot exceed 100 commits per version
        commit_response = requests.get(url+"/api/v4/projects/"+project_id+"/repository/commits", headers={'PRIVATE-TOKEN': private_token}, params={'ref_name': branch, 'until' : date_until, 'per_page' : 100})
        #Response in the form of a list of dictionnaries
        commitList = commit_response.json()
    
    #If it isn't then we take all commits between this version and the precedent one
    else :
        date_since = tagList[current_version_index + 1]['commit']['committed_date']
        
        #API request to get the list of all the commits - cannot exceed 100 commits per version
        commit_response = requests.get(url+"/api/v4/projects/"+project_id+"/repository/commits", headers={'PRIVATE-TOKEN': private_token}, params={'ref_name': branch, 'until' : date_until, 'since' : date_since, 'per_page' : 100})
        #Response in the form of a list of dictionnaries
        commitList = commit_response.json()
    
    #List containing all the titles of the concerned commits
    List_of_commits = [commit['title'] for commit in commitList[:-1]]
    
    return List_of_commits

def releaseNote(List_of_commits) : #RMT-3
    """
    
    Parameters
    ----------
    List_of_commits : list
        The list of all the commits of the version

    Returns
    -------
    List_of_tickets : list
        The list of all the Jira tickets of the version

    """
    # Private variable containing all the tickets at the start of the commits' title
    tickets_set = set()

    for title in List_of_commits:
        ticket = re.search(reg_ex_id, title)
        if ticket:
            tickets_set.add(ticket.group())
    
    # Private variable containing all the tickets at the start of the commits' title in a sorted manner
    List_of_tickets = sorted(tickets_set)
    
    return List_of_tickets

#Choice of the right program to execute
List_of_versions, tagList = versions()

if command == "versions" :
    for version in List_of_versions :
        print(version)
else :
    if version_number in List_of_versions :
        List_of_commits = commits(List_of_versions, tagList, version_number)
        if command == "commits":
            for commit in List_of_commits :
                print(commit)
        elif command == "releaseNote":
            List_of_tickets = releaseNote(List_of_commits)
            for ticket in List_of_tickets :
                print(ticket)
    else :
        print("Incorrect version number")